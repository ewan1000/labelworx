---
title: "Watermarking Pdfs"
date: 2021-08-17T13:53:52+02:00
draft: false
tags: [javascript, regex, Acrobat]

---

## How to add a PDF watermark with Javascript

Sometimes you need to add headers on your PDFs. Sometimes it's a drag to have to type the same thing each time. Sometimes you mindlessly introduce a typo and it gets bounced back from a reviewer later... ugh.

I wanted to get rid of having to think about it too much.

The javascript below can be executed from a [Custom Command or Action](https://helpx.adobe.com/acrobat/using/action-wizard-acrobat-pro.html), to be run over one or many documents.

The example below pulls information from the file name string (using Regular Expressions) and your Acrobat Identity Name (`Preferences>Identity`).

: Regex:
| Find | Replace |
| --- | --- |
| `^.*?(PR# ?\d+)[^\d].+$` | `$1` |
| `^.+?(Att[^\d]+\d\d?).+$` | `$1` |

With the file name `"PR# 123456 Att#1 [filename].pdf"` the watermark generated will be:

```
PR# 123456 Att#1 Wan 17 Aug 2021 Page [X] of [Y]

Company Confidential Internal Use Only
```

```javascript
/* 

Attachment Header v0.2
Wan
17 August 2021

 */

/* Adds Company confidentiality string as watermark to bottom of page */

this.addWatermarkFromText({ 
cText: "Company Confidential Internal Use Only", 
nFontSize: 12, 
nVertAlign: app.constants.align.bottom, 
nVertValue: 36, 
aColor: color.red,
nOpacity: 0.5
});


/* 

== Header is added to top left of page, it's calculated as follows: ==
- PR# and Attachment string from document filename 
- "Name" from Acrobat Identity fields; see: 'Edit>Preferences>Identity'
- calculates date and page numbers

*/

// Set PR# and Att# variables

var myPR = this.documentFileName.replace(/^.*?(PR# ?\d+)[^\d].+$/, "$1"); 
var myAtt = this.documentFileName.replace(/^.+?(Att[^\d]+\d\d?).+$/, "$1"); 


// loop through pages, build and apply string

for (var p = 0; p < this.numPages; p++) {
this.addWatermarkFromText({ 
cText: myPR + " - " + myAtt + " " + identity.name + " " + util.printd("dd.mmm.yyyy", new Date()) + " " + "Page " + (p + 1) + " of " + this.numPages, 
nFontSize: 12, 
nHorizAlign: app.constants.align.left, 
nHorizValue: 36, 
nVertAlign: app.constants.align.top, 
nVertValue: -50, 
aColor: color.red, 
nOpacity: 0.5, 
nStart: p}
)
;
}
```