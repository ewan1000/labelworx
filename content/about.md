---
title: About me
subtitle: Why you'd want to hang out with me
comments: false
---

## Who am I?

I work in Life sciences in Germany. I'm not a  designer or a programmer, I just get bored easily.

This website is about:

- Science
- Documentation
- Technology
- Anything else I'm interested in

I'm saving my notes here for me to find for when I inevitably forget something. 

Maybe you'll find something useful here too?